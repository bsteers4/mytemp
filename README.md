
MyTemp by Bruce Steers

Written in Gambas basic

A simple temperature display using LCDLabel

 Gets temperature in one of 4 ways, 
* Select a file
* run a command like 'sensors' and uses it's direct output or 
* auto select hottest zone
* manually select zone


You can use a shell command like sensors (from package lm-sensors)
Use 'sudo apt-get install lm-sensors' or use your package manager to install the 'sensors' command.

**Command Example...**

$ 'sensors|grep \'Core 0\'|awk \'{print $3}\''

The above command runs 'sensors' and uses grep to find the line with 'Core 0' in it that for me looks like this...
Core 0:       +45.0°C  (high = +82.0°C, crit = +100.0°C)

and then uses awk to get the 3rd bit of text '+45.0°C' (space separation makes 'Core 0:' 2 words) 


Options...
Set colors.
Let main text colour change according to temperature
Show on all workspaces
Show on all screens
